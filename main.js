// Requiredments
const electron = require('electron');
const url = require('url');
const path = require('path');
const fs = require('fs');
const windows = { main: false, tray: false };

var FLAG_DEV = false;

// Location ands defaults
const appDataDir = function(){
	var path = electron.app.getPath( 'appData' ) + '/Fontle';
	if( !fs.existsSync( path ) ){
		fs.mkdirSync( path );
	}
	return path;
}();
const usrDataDir = electron.app.getPath( 'userData' );
const usrHomeDir = electron.app.getPath( 'home' );
const dbTemplate = { 
	empty: true,
	installPath: usrHomeDir + '/Library/Fonts',
	libraryPath: usrHomeDir + '/Fonts',
	keepLibrarySorted: true,
	fonts: []
};
const dbLocation = function(){
	var path = appDataDir + '/database.json';
	if( !fs.existsSync( path ) ){
		fs.writeFileSync( path, JSON.stringify( dbTemplate ) );
	}
	return path;
}();
const log = function(){
	var path = appDataDir + '/log.txt';
	if( !fs.existsSync( path ) ){
		fs.writeFileSync( path, `[${new Date().toString()}] - Log created \n` );
	}
	return function( message ){
		fs.appendFile( path, `[${new Date().toString()}] - ${message} \n`);
	}
}();




electron.app.on('ready', function(){

	log( 'App Started' );
	
	
	function showMainWindow(){
		var b = windows.tray.getBounds();
		var w = windows.main.getSize();
		windows.main.setPosition(
			b.x - w[0] / 2,
			b.y + b.height
		);
		windows.main.show();
	}
	
	windows.tray = new electron.Tray(path.join( __dirname, 'tray.png'));
	windows.tray.setPressedImage(path.join( __dirname, 'tray-highlight.png'));  
	windows.tray.on('click', function( event ){
		if( windows.main.isFocused() ){
			windows.main.hide();
		} else {
			showMainWindow();
		}
	});
	windows.tray.on('drop-files', function( event, files ){
		windows.main.show();
		windows.main.send( 'drop-files', files );
	});
	
	windows.main = new electron.BrowserWindow({
		width: FLAG_DEV ? 700 : 400,
		height: 400,
		show: false,
		frame: false,
		resizable: false,
		transparent: true,
		hasShadow: false,
		alwaysOnTop: true
	});
	
	windows.main.loadURL(url.format({
		pathname: path.join( __dirname, 'interface.html' ),
		protocol: 'file:',
		slashes: true
	}));
	
	windows.main.on('blur', function(){
		// Maybe add class to page to get fadein animation
		// fadeout with waiting for timeout would be fine too.
		windows.main.hide();
	});
	
	if( FLAG_DEV ){
		windows.main.webContents.openDevTools()
	}
	
	electron.app.dock.hide();
	showMainWindow();
	
});
	
electron.app.on('activate', function(){

	if( windows.main ) windows.main.show();
	
});

// Allow the main logger to log any events
electron.ipcMain.on('log', function( event, message ){
	
	log( message );
	
});
electron.ipcMain.on('get-database', function( event ){
	
	fs.readFile( dbLocation, function( err, json ){
		json = json ? json.toString() : JSON.stringify( dbTemplate );
		event.sender.send( 'get-database', null, json );
	});
	
});
electron.ipcMain.on('save-database', function( event, data ){
	
	if( data.empty ) delete data.empty;
	data.lastUpdate = new Date().toString();
	
	fs.writeFile( dbLocation, JSON.stringify(data), function( err, success ){
		event.sender.send( 'save-database', err, success );
	});
	
});
electron.ipcMain.on('reset-database', function( event, data ){
	
	fs.writeFile( dbLocation, JSON.stringify(reset), function( err, result ){
		event.sender.send( 'get-database', err, JSON.stringify( reset ) );
	});
	
});
electron.ipcMain.on('development-active', function( event, data ){
	event.sender.send( 'development-active', null, FLAG_DEV );
});